library(tidyverse)
n_actual <- 108
dat <- read_csv("custom-R-code/data/mark-recapture-demo2.csv")
dat |> 
  mutate(Pop_estimate = as.numeric(Pop_estimate),
         N_marked = as.factor(N_marked),
         N_recaptured = as.factor(N_recaptured)) |> 
  na.omit() |> 
  group_by(N_marked, N_recaptured) |>
  mutate(mean_est = mean(Pop_estimate, na.rm = T),
         sem_est = mean_est/sqrt(n()-1)) |> 
  ggplot(aes(x = N_marked, y = mean_est,
             ymin = mean_est-sem_est,
             ymax = mean_est+sem_est,
             fill = N_recaptured)) + 
  geom_pointrange(position = position_dodge(0.5), size = 1, shape = 21) +
  # geom_point(aes(y = Pop_estimate), position = position_dodge(0.5), shape = 21) +
  geom_hline(yintercept = n_actual, linetype = 'dashed') +
  scale_fill_manual(values = c("olivedrab2", "darkgoldenrod3", "sienna4")) +
  theme_classic()
