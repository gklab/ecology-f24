---
title: ''
author: "Gaurav Kandlikar"
show_post_date: no
layout: "single-sidebar"
type: "singlepage-TOC"
sidebar_left: yes
details: false
---

***Download PDF of Semester Project Overview [here](/semester-project/semester-project-overview.pdf)***

***Download PDF of Potential Communities assignment [here](/semester-project/semester-project-potential-communities.pdf)***

<!--

***Download PDF of Semester Final Guidelines [here](/pdf/semester-project-final-guidelines.pdf)***

***Download PDF of Semester Project Overview [here](/pdf/semester-project-self-eval-template.pdf)***
-->

## Semester Project Overview

### Objective

As we have started discussing in class, we are living in a weird time to be an ecologist. Thanks to technological advances, ecologists now have access to data and computational power on a scale that we couldn't have imagined decades ago. At the same time, the consequences of global change are making themselves felt in all aspects of our life. Given this context, ecologists needs to be *persistent* and *creative* in thinking about how the fundamental question that drive our field relates to the environmental challenges unfolding around the world. 

Thus, the format for this course's Semester Project is an "*UnEssay*" assignment (see [References](#references) for an overview of unessays in general). <mark>***The prompt for the unessay*** is to engage your curiosity by picking a natural system anywhere in the world, and explore+communicate its ecology and environmental challenges in any format you wish.</mark> 

Your unEssay should provide the audience an overview of how the four central themes in ecology introduced in the course[^1] play out in your focal system. In addition to general information about the system that you can gather from any number of reputable sources (textbooks, scholarly websites, etc.), I also expect that your unEssay be inspired by and reflect the details of at least **three peer-reviewed academic articles**. 



[^1]: In this class, we are focusing on the following themes in ecology: <br> - Ecological systems are ***dynamic***, meaning that their properties can change over time.<br> - Ecological systems feature ***feedacks***, meaning that the dynamics of one component of a system often affects another. <br> - The dynamics and wellbeing of ecological systems are tightly intertwined with the dynamics and ***wellbeing of human societies***. <br> -  Understanding ecological systems requires us to confront ***uncertainty***, which can arise because of limited knowledge of the system, or due to inherently stochastic processes.



Just as some examples, you might decide to write and illustrate a children's book inspired by the wetlands of Louisiana, record a podcast episode inspired by the forests of Hawaii, 3-D print a sculpture inspired by mangroves in Indonesia, or write a song inspired by the Alaskan tundra. Your natural system can also be an [urban area](https://www.esa.org/urbanecology/resources/journals/). As you select a focal community, I encourage you to think about what resources (including published ecological literature) you can find about that area to help you put together a project. We will have scheduled time in class to discuss potential places and to get feedback from peers and from me.

To reiterate - the possibilities above are just examples; the goal here is to tap into your creative strengths and curiosities, and develop a project that helps you communicate about the ecology and environmental challenges of a place of your choosing. You can also choose to write a traditional 8-10 page essay, if that is where your strengths lie. 

For more background on why we are adopting this UnEssay format for this course, please refer to works listed in the [References](#references) section at the end of this document. 

### Assignment structure

While the format details are almost largely up to individual students, I will provide considerable structure over the coming months to help you put together a compelling semester project. 

Early on in the semester, you will be asked to write brief narratives in which you explore potential **places** that will be the focus of your project, and potential **formats** for your project. You can also choose to turn these in as video/audio recordings rather than written narratives.

Later in the semester, you will be asked to submit an **annotated bibliography** of peer-reviewed papers that you will read to help you develop the project's contents, and submit a **formal proposal** that brings all the pieces together.  

At the end of the semester, during the schedule final exam time for this course, we will have a "project fair" in which you share your semester project with your peers (and potentially with other members of the LSU/Biological Sciences community). 

Along with the final project, you will also submit a *video recording or essay* explaining the thought process behind your project, what you learned about the ecology and environmental challenges in your focal area, and how your creative work represents this learning. 


### Evaluation

*Much of the text in this section is adopted from [Dr. Daniel O'Donnell](https://people.uleth.ca/~daniel.odonnell/Teaching/the-unessay#topic)*

Given the wide room for defining your own focal community, and your own medium of presentation, how is this project graded?  The main criterion is how well the project fits together. That is to say, how *compelling* and *effective* your work is.

An unessay is *compelling* when it shows some combination of the following:

- it is as interesting as its topic and approach allows
- it is as complete as its topic and approach allows (it doesn't leave the audience thinking that important points are being skipped over or ignored)
- it is truthful (any questions, evidence, conclusions, or arguments you raise are honestly and accurately presented)

In terms of presentation, an unessay is *effective* when it shows some combination of these attributes:

- it is readable/watchable/listenable (i.e. the production values are appropriately high and the audience is not distracted by avoidable lapses in presentation)
- it is appropriate (i.e. it uses a format and medium that suits its topic and approach)
- it is attractive (i.e. it is presented in a way that leads the audience to trust the author and their arguments, examples, and conclusions).

#### Assigning points

The Semester Project comprises one-third of your grade for this course (60 points). Of these, half will come from check-in assignments throughout the semester (see [Timeline](#timeline) for details). I will provide guidelines for each of these check-in assignments as we approach their submission dates.  

The second half of the points will come from the final project. As part of the essay/recording explaining your process for the final project, you will be asked to propose a grade, out of 30 points that you think your project has earned. This score should be based on various categories that reflect the goals of this assignment:

- how *compelling* and *effective* is your project at conveying the ecology and the environmental challenges of your focal area?
- how much have you learned about the ecology and environmental challenges in your focal area from doing this project?
- to what extent did you apply creative thinking to represents this learning?
- how closely does your project match the work you had proposed to do in the mid-semester "Formal proposal" stage? (*If there is a substantial difference between your proposal and final project, that is OK - I would just like you to explain the divergence*.)

I trust students to be honest in your evaluation of your own work. I will also independently assess the work using the same rubric. When I feel students have under-estimated their project's creativity, effectiveness, creativity, or value, I may choose to boost your project score directly. If for some reason I feel there is a substantial mismatch between your project and your assessment of it, I may request a conversation in-person or over zoom to discuss the perceived mismatch. 

### Timeline

***Sunday, September 22th***: List of potential focal places/ecological communities due. (5 points)

***Sunday, October 6th***: List of potential formats due. (5 points)

***Sunday, October 27th***: Annotated bibliography of peer-reviewed papers you plan to incorporate into your work. (5 points)

***Sunday, Nov. 17th***: Formal proposal due. (15 points)

***Monday, Nov. 25th*** and ***Tuesday, Nov. 26th***: Project checkins with Gaurav

***Tuesday, December 10th***: Project due; Project fair at 5.30 pm (30 points)


### References 

1. Daniel O'Donnell, 2012. "The unessay." https://people.uleth.ca/~daniel.odonnell/teaching/the-unessay

2. Emily Suzanne Clark. 2016. "The Unessay." https://esclark.hcommons.org/the-unessay/. 

3. Patrick Sullivan. 2015. "The UnEssay: Making Room for Creativity in the Composition Classroom." *College Composition and Communication* 67:1, 6-34. 
