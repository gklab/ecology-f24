---
title: ''
author: "Gaurav Kandlikar"
show_post_date: no
layout: "single-sidebar"
type: "singlepage"
sidebar_left: yes
details: false
---

For students in Biol 4253, please contact me by email (gkandlikar@lsu.edu) rather than through the Moodle messaging portal. 

If you are interested in making a fork of this website for your own course, please check out the website source code on [GitLab](https://gitlab.com/gklab/ecology-f24). Feel free to make an Issue on gitlab or email me at gkandlikar@lsu.edu if you have any questions. Even if you don't have any questions, I would love to hear from you if you find this website useful!