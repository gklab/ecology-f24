---
title: ''
author: "Gaurav Kandlikar"
show_post_date: no
layout: "single-sidebar"
type: "singlepage-TOC"
---

***Click [here](biol4253-syllabus.pdf) for a PDF version of this syllabus.***


## Welcome to Principles of Ecology!
<div class="row">
  
  <div class="column">
  
My name is Dr. Gaurav Kandlikar (he/him/his), and I am so excited to share this class with you. I'm an Assistant Professor in the Department of Biological Sciences, and I'm looking forward to get to know you all over the coming months.

[My lab](https://gklab.org)'s research focuses on plant community ecology, which means we study how plants interact with the environment and with other organisms in their environment. A lot of my work has especially focused on how plants interact with the soil microbiome. I love my job because it has given me the opportunity to visit and learn about amazing places and meet amazing people from all over the world. In my free time I enjoy running/biking, cooking, and exploring new music. 

To contact me about this course, please [send me an email](mailto:gkandlikar@lsu.edu). I will respond within 48 hours unless your email concerns an urgent matter. My response times will be slower on the Moodle message portal, so I suggest you avoid that entirely. 

</div>

<div class="column">
    <img src="GSK_profile.jpg" style="width:800px; padding-top:15px;padding-left:25px;">
</div>
</div>


## Course overview

Ecology as a discipline is motivated by human efforts to describe, understand, predict, and modify nature. Specifically, a lot of work in ecology is interested understanding the various interactions that shape where organisms live and how their abundances change over time. This course will introduce you to the questions that motivate the field of ecology, the approaches that ecologists take to answer these questions, and the implications of ecological research for ongoing environmental challenges. 


### Learning objectives

Our goal over the next semester will be to develop skills to:

1.  Describe how the field of ecology tackles the complexity of nature
2.  Explain how mathematical thinking helps generate ecological insights  
3.  Interpret figures and results from published ecological literature  
4.  Discuss the role of ecology in addressing major societal challenges.

### Course content and core themes

The content of the course is broadly organized around different levels of ecological organization:

1. We will begin by addressing how individuals interact with the abiotic environment and with other members of their own ***population***. 
2. We will then focus on how different species interact with one another, and how this affects ecological ***communities***. 
3. Then, we will learn about the ecology of entire ***landscapes*** and ***ecosystems***. 

At each scale, we will highlight four aspects of natural systems and ecological research:

1. Ecological systems are ***dynamic***, meaning that their properties can change over time.
2. Ecological systems feature ***feedacks***, meaning that the dynamics of one component of a system often affects another.
3. The dynamics and wellbeing of ecological systems are tightly intertwined with the dynamics and ***wellbeing of human societies***  
4. Understanding ecological systems requires us to confront ***uncertainty***, which can arise because of limited knowledge of the system, or due to inherently stochastic processes.

Throughout the semester, we will also discuss how basic ecological knowledge can help us make sense of -- and deal with -- some of the most pressing ecological challenges that we face today. 

<!--
The course schedule is also designed with substantial in-built flexibility to address your own objectives for the semester, which I will ask you to write about in your first weekly self-reflection essay.
-->

## Course structure and evaluation

The goal of this course is to help you master the fundamentals of ecology, build a framework for how ecological principles relate to ongoing ecological crises, and develop skills that will help you apply fundamental concepts to action.

Traditional grading, which emphasizes one-size-fits-all assignments and high-stakes evaluations, can be an [imperfect system](https://www.chemedx.org/blog/ungrading-what-it-and-why-should-we-use-it?ref=jessestommel.com) for evaluating student progress towards such goals. Thus, rather than reflecting your performance on high-stakes exams and projects, your final grade in this course will reflect your effort and learning in three areas:  

- 60 points (1/3rd of the course total) for [Self Reflection](#self-reflection) 
- 60 points (1/3rd of the course total) for [Biweekly Activities](#weekly-activities) 
- 60 points (1/3rd of the course total) for the [Semester Project](#semester-project) 

Students will be in charge of self-grading the self reflection, Dr. Kandlikar will grade biweekly activities, and grades for the semester project will be decided collaboratively between the student and instructor. **Please expand the boxes in the sections below for more information on how self-grading will work in this class**. We will also devote time during Week 1 to ensure that all students are familiar with this system and understand why this course is adopting this approach.


There will be readings and/or other materials (e.g. Youtube videos) listed for each week. I expect students to complete these before coming to class on Monday. 

### Self Reflections {#self-reflection}

**Every other week starting in Week 2**, you will be asked to complete a self-reflection activity in which you take a step back from the content that we are learning in the course, and instead think about how you are growing as a scientist and person. While you will have considerable free reign in what you choose to reflect on, some themes you might explore are questions like: Is there something we learned that challenges your previous beliefs about how nature works? Are there particular ideas that you find hard to learn, and what are some roadblocks in your way that we could help overcome? How do events outside of the course affecting you learning of course material, or your life as a student more generally?

As a general benchmark, I have found that meaningful reflections can take somewere between 700-900 words. Please note that these are not 'minimum' or 'maximum' word counts; I am suggesting this range just to help you know how much effort I am hoping you put into this activity.  More details are available in the box below (click to expand).


<div class="div-details">
<div class="div-indent">
<details>
<summary>Click here for more details about Weekly Reflections
</summary>

#### Why are we doing this?

The goal for this activity is to help us develop a practice of critical self-reflection.[^1] Such self-reflection, in which we regularly dedicate time for setting goals, monitor and reflect on our growth, and use previous experiences to shape our future, is a key ingredient in [meta-cognition](https://cft.vanderbilt.edu/guides-sub-pages/metacognition/), and can help foster long-term and sustained learning. In addition to helping you reflect on your learning, your weekly reflections will also help me better understand how the course is progressing, and help identify ways for the class to be more inclusive and impactful for all students.

A second motivation for this activity is to encourage you to think freely and broadly about how the things we learn in class interact with your lives outside the class. For example, I encourage you to use these self-reflections as a space to think about how the topics we cover in class relate to societal challenges . Equally importantly, I also encourage you to reflect on how the field of ecology itself can learn, grow, and change based on what is happening in the world.

#### Who will read my self-reflection?

You should consider yourself as the primary audience for your self-reflection. However, you should not consider this as an entirely private venue: I plan to regularly read students' self-reflection essays to get a sense of how things are progressing, and to identify areas of potential improvement/adjustment in the course schedule. 

#### How are weekly reflections graded? {#weekly-reflection-grading}

Weekly reflections are meant as a personal endeavor that center your own learning and growth. Thus, you will be in charge of grading your own weekly reflections. Specifically, every week starting in Week 4, you will be asked to return to re-read your previous reflection and self-assign a grade out of 10 points. Through the semester, each student will complete 7 reflections, for a total of upto 60 points. (The lowest self-reflection score will be dropped, such that each student effectively has a "free pass" to skip a reflection). 

#### How should I grade my self-reflection? {#how-to-grade-reflection}

There is no set format for what makes for a good reflection essay. Reflections can be retrospective (thinking back over the past) or prospective (thinking ahead to your future); narrow or broad; long or short; hopeful or pessimistic; vague or specific. What defines a good reflection is whether it best captures your state of mind and helps you grow as a learner. While I will not be reading your essays with an eye towards grading, I reserve the right to request a one-on-one discussion if there are consistent disparities between how I would have graded your essay and your self-assigned score.


[^1]: I think of critical self-reflection as the act of consciously thinking about our past, present, and future selves to ensure that we are getting the most out of our lives

</details>
</div>
</div>


### Biweekly Activities {#weekly-activities}

**Every other week starting in Week 3**, you will be asked to complete and upload an activity sheet related to course content. Activities will vary across the semester, but typically involve involve some combination of engaging with primary ecological literature, engaging with quantitative topics we cover in class, and/or engaging with ecological ideas through other mediums like podcasts or news stories. **All Biweekly Activity will be due on Moodle by Sunday night**. Through the semester, each student will complete 7 Biweekly Activities, for a total of upto 60 points. (The lowest activity will be dropped, such that each student effectively has a "free pass" to skip an activity). 


<div class="div-details">
<div class="div-indent">
<details>
<summary>Click here for more details about the Weekly Activities
</summary>


#### How much time should I expect each Biweekly Activity to take?

I expect most bi-weekly activities to take ~4 hours to complete. In some weeks, you will be asked to share progress on the [semester project](#semester-project) in addition to completing the weekly assignment; in these weeks, the weekly assignments will be lighter. 

#### How will Biweekly Activities be graded?

Each Biweekly Activity will be worth 10 points. Dr. Kandlikar will grade your Activity submissions according to a rubric that will be uploaded to Moodle. **Note** that students will have an opportunity to earn back any points missed in the initial Activity submission by completing an alternate version of the Activity. 

</details>
</div>
</div>

### Semester project {#semester-project}

Over the next few months, you will work towards a semester project in which you get to know and describe the ecology of an area of your choosing. The format of the project can be of your choosing - e.g. you may choose to make an infographic, record a podcast episode, write and illustrate a children's book, compose a new song. (This list is not meant to be comprehensive! I encourage you to use this as an opportunity to develop other media/communication skills you are interested in.)

We will discuss details of the semester project in class during the second week of class. 

### Late submissions  

Given the rapid pace of the semester, I expect students to complete all submissions according to the stated due-dates. Each self reflection comes with a 24-hour grace period; biweekly activities do not include such a blanket grace period. As explained above, the lowest biweekly reflection score and biweekly activity will be dropped during grade calculations, meaning that in effect, each student is allowed a "free pass" on one self reflection, and one weekly activity - no questions asked.  Beyond the grace periods and free pass, if an illness or other life event delays your submission of a reflection or activity, please communicate with me as soon as possible. 

I expect all assignments related to the semester project to be submitted on time. As above, if an illness or other life event delays your submission, please talk to me in person and/or over moodle.

### Weekly coworking sessions

I love meeting with students outside of class hours to work through course content, to explore ideas that extend beyond what we have to discuss in the classroom, or chat about anything else on your mind. Times for weekly coworking sessions will be announced in Week 2 of the semester, following a survey in Week 1.

### Letter grades

At the end of the term, letter grades will be assigned as follows:

```
    A+ for earning 175-180 points over the semester;    
    A  for earning 168-174 points over the semester;  
    A- for earning 162-167 points over the semester;   
    B+ for earning 157-161 points over the semester;  
    B  for earning 150-156 points over the semester;   
    B- for earning 144-149 points over the semester;   
    C+ for earning 139-143 points over the semester;  
    C  for earning 132-138 points over the semester;   
    C- for earning 126-131 points over the semester;  
    D+ for earning 121-125 points over the semester;  
    D  for earning 114-120 points over the semester;
    D- for earning 108-113 points over the semester;  

    and F for earning fewer than 108 points over the semester. 
```

## Calendar

***Daily schedule may change. Please check this website for the most recent version!***


| Week/Date | Topic | Resources | Activity | Self reflection | Semester project |
| ---------  | ----- | ----- | ---------- | --------------- | ---------------- |
| Week 1 <br> (26 Aug) | [Introduction to ecology](/weekly-activities/week-01/)| [What is Ecology?](/week1/pdf/QuantEco-Ch1.pdf) | - | -  | -  |
| Week 2 <br> (2 Sept) <br> *No Mon. class* | Population Ecology<br>Part 1: how populations grow (or shrink)| [Intro to Population Ecol](/week2/pdf/popln-ecology-reading.pdf) *(Sections 45.1 & 45.3)*;<br>[Exponential growth dynamics](https://lsu.box.com/s/py9pj4cw8o8bk7hreombejpndrl223rr)| -  | [link](https://moodle.lsu.edu/mod/assign/view.php?id=2483509)<br>(Due Sep 8)| Overview of semester project presented in class  |
| Week 3 <br> (9 Sept) | Population Ecology<br>Part 2: Stage-strucured population dynamics  | [Intro to Leslie Matrices](https://www.youtube.com/watch?v=I5WM2wdjr1M)|  [link](https://moodle.lsu.edu/mod/assign/view.php?id=2483511)<br>(Due Sep 15)  | - |  - |
| Week 4 <br> (16 Sept)  | Population Ecology<br>Part 3: Density dependence | [Density-dependent population growth](https://lsu.box.com/s/ayadcjn5wglnjlpwwja9k0maq6lsr9je) |   - |[link](https://moodle.lsu.edu/mod/assign/view.php?id=2483512)<br>(Due 22 Sept) |[List of potential focal communities](/semester-project/semester-project-potential-communities.pdf)<br>(Due 22 Sept)|
| Week 5 <br> (23 Sept)   | Community Ecology Part 1 | tbd|  [link] | - | - | 
| Week 6 <br> (30 Sept)   | Community Ecology Part 2 |tbd|  - | [link] | List of potential media/formats due |
| Week 7 <br> (7 Oct)   | Community Ecology Part 3 | tbd|  [link] |  - |- |
| Week 8 <br> (14 Oct)  <br> *No Fri. class*  |Community Ecology Part 4|  tbd | -  | [link] | - |
| Week 9 <br> (21 Oct)   | Mid-semester check-in| tbd| [link] | - | Annotated Bibliography due|
| Week 10 <br> (28 Oct)   | Landscape Ecology<br>Part 1 |tbd|  - | [link] | - | 
| Week 11 <br> (4 Nov)   | Landscape Ecology<br>Part 2: Measuring turnover in biodiversity | tbd| [link] | -  | - |
| Week 12 <br> (11 Nov)   | Ecosystem Ecology Part 1 |tbd|  -  | [link] | Formal Proposal due |
| Week 13 <br> (18 Nov)   | Ecosystem Ecology Part 2 | tbd| [link]  | -  | - |
| Week 14 <br> (25 Nov)  <br> *No W/F class*   | *Thanksgiving week* | - | - | [link] | Project check-in with Gaurav (Monday in class or Tuesday on campus) |
| Week 15 <br> (2 Dec)   | Ecology and Me| tbd|  [link] | - | - |
| Week 16 <br> (Dec 9)   | Alloted final slot: Tues. Dec 10 from 5.30pm--7.30pm | - |  - | - | Semester project round-robin |


## Course Context

### Student health and wellness

Your mental and physical health are important to me. If you find yourself struggling with your mental or physical health this semester, please feel free to approach me and I will do my best to flexible and accommodating with the class, and to help connect you to professional support on campus. You can also find support for both mental and physical health concerns through LSU's [Student Health Center](https://www.lsu.edu/shc/index.php). 

### Accessibility statement

My goal is to help you learn and achieve your goals in this class. If you have any mental or physical health difficulties that might affect your engagement with the course, please contact me as soon as you can. I also encourage you to see a [staff member in Disability Services](https://www.lsu.edu/disability/about/staff.php), who can help explore more formal course accommodations.

### Diversity, Equity & Inclusion our classroom

*Adapted from the [LSU Diversity Statement](https://lsu.edu/diversity-statement/index.php)*  

We believe diversity, equity, and inclusion enrich the educational experience of our students, faculty, and staff, and are necessary to prepare all people to thrive personally and professionally in a global society. Therefore, we are firmly committed to an environment that affords respect to all members of our community. We will work to eliminate barriers that any members of our community experience.

To make our community a place where that can happen, we must recognize and reflect on the inglorious aspects of our history. Throughout this course, we will acknowledge the need to confront the ways racism, sexism, ableism, ageism, classism, LGBTQ+ phobia, intolerance based on religion or on national origin, and all forms of bias and exploitation have shaped our everyday lives.

### Academic integrity

*Adapted from [UC Berkeley Center for Teaching and Learning](https://teaching.berkeley.edu/statements-course-policies)*

You are a member of an academic community at one of the world’s leading research universities, and I hold all students in this class up to the high standards of academic integrity this entails. Universities like LSU create knowledge that has a lasting impact in the world of ideas and on the lives of others; such knowledge can come from an undergraduate project as well as the lab of an internationally known professor.  One of the most important values of an academic community is the balance between the free flow of ideas and the respect for the intellectual property of others. Researchers don't use one another's research without permission; scholars and students always use proper citations; professors may not circulate or publish student papers without the writer's permission; and students may not circulate or post materials (handouts, exams, syllabi--any class materials) from their classes without the written permission of the instructor. These same principles apply to work generated from Artificial Intelligence tools like ChatGPT: I acknowledge these as potentially useful tools to help us learn and generate knowledge about our world, but I ask that you cite its use when you do use it, and that you don't abandon your own creativity and caution when you incorporate its outputs. 

Any material submitted by you and that bears your name is presumed to be your own original work that has not previously been submitted for credit in another course unless you obtain prior written approval to do so from your instructor. In all of your assignments, including your homework or drafts of papers, you may use words or ideas written by other individuals in publications, web sites, or other sources, but only with proper attribution. If you are not clear about the expectations for completing an assignment or taking a test or examination, be sure to seek clarification from your instructor beforehand. Finally, you should keep in mind that as a member of the campus community, you are expected to demonstrate integrity in all of your academic endeavors and will be evaluated on your own merits. *The consequences of cheating and academic dishonesty—including a formal discipline file, possible loss of future internship, scholarship, or employment opportunities, and denial of admission to graduate school—are simply not worth it*.


### Land Acknowledgement Statement

We would like to acknowledge the indigenous history of Baton Rouge, and more broadly, Louisiana as part of our responsibility to acknowledge, honor, and affirm indigenous culture, history and experiences. We recognize the communities native to this region including the Caddo Adai Indians of Louisiana, Biloxi Chitimacha Confederation, Chitimacha Tribe of Louisiana, Choctaw Nation, Coushatta Tribe, Four Winds Cherokee Tribe, Muscogee (Creek), Point au Chien Tribe, Tunica Biloxi Tribe, United Houma Nation, and others whose memories may have been erased by violence, displacement, migration, and settlement. We thank them for their strength and resilience as stewards of this land and are committed to creating and maintaining a living and learning environment that embraces individual differences, including the indigenous peoples of our region.

For more information on the indigenous peoples of Louisiana, please see the [LSU Libraries Lib Guide](https://guides.lib.lsu.edu/c.php?g=1095533&p=7990126) or one of the links at [this site](https://www.lsu.edu/chse/sis/resources/aeri/acknowledgement.php). To identify the indigenous communities in your local area, please see the [Native Land Map](https://native-land.ca/).

### Labor Acknowledgement Statement

We would like to acknowledge that much of the culture, economic growth, and development of Baton Rouge, Louisiana State University and Louisiana as a whole, was built on the labor of enslaved people, primarily of African descent, who were kidnapped and brought to Louisiana into chattel slavery. We wish to acknowledge their labor and honor their lives and legacy, and that of their descendants, who suffered continued harm as the result of Jim Crow and other forms of entrenched white supremacy that continue today. Although technically ending slavery following the adoption of the 13th Amendment to the U.S. Constitution, we recognize its continued impact felt by those forced to work by violence, threats, and coercion.  

Louisiana State University sits on three former plantations: Arlington, Nestle Down, and Gartness. According to the LSU student public history project [Slavery in Baton Rouge](https://slaverybr.org/the-former-plantations-of-lsus-campus/):

> *What is now the central campus of LSU was once the location Gartness Plantation’s cabins, storehouses, and master’s house. However, the site where Arlington Plantation’s house once stood has since been eroded by the Mississippi. Gartness Plantation’s property lines closely line up with central campus. The Arlington Plantation starts near LSU’s Vet School and ends at what Brightsideightside Drive. The Gartness plantation, Magnolia Mound, D. Daigre, and J. H. Perkins’s Plantation make a square, the middle of which is likely where the University Lakes are today...The lives of the enslaved people who once lived on Arlington, Nestle Downs, and the Gartness Plantations should not be forgotten. It is important to identify that plantation slavery was prevalent in the area during the Antebellum Era and that these plantations had an impact that can still be seen throughout modern Baton Rouge. Names such as Arlington and Gourrir are still used throughout the area in the locations where the former properties of these plantations existed.*

(Adapted from the work of [Dr. TJ Stewart](https://www.diverseeducation.com/demographics/african-american/article/15108677/on-labor-acknowledgements-and-honoring-the-sacrifice-of-black-americans) & [Solid Ground](https://www.solid-ground.org/))

Thanks to LSU [Archival Education and Research Institute](https://www.lsu.edu/chse/sis/resources/aeri/) as the source of these statements