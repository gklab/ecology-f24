---
action_type: text
description: <b>Welcome to Principles of Ecology!</b> <br> I am thrilled that you are here, and I am excited to share this class with you over the semester. <br> <br> 
.image_left: false
images:
- img/arch.jpg
show_action_link: true
show_social_links: false
text_align_left: false
title: Principles of Ecology
subtitle: Biol 4253 at LSU, Fall 2024, with Dr. Gaurav Kandlikar
type: home
action_label: Read the syllabus &rarr;
action_link: /syllabus
---


<!---->

** index doesn't contain a body, just front matter above.
See index.html in the layouts folder **
