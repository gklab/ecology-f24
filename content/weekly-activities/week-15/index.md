---
date: "2021-01-01"
draft: true
type: collection
excerpt: Semester retrospective
subtitle: ""
title: Week 15
weight: 15
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-15/slides.html
---

## Overview

This week, we will do a retrospective of the semester to summarize what we have learned, how these topics relate to challenges touching each of our lives, and how you can apply this knowledge moving forward.

Image of a reflective lake in Austria by by [Christian Holzinger](https://unsplash.com/@pixelatelier?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/photography-of-trees-and-body-of-water-under-white-sky-during-daytime-bVOmGEOnEzc?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)