---
date: "2021-01-01"
draft: false
type: collection
excerpt: Measuring biodiversity
subtitle: ""
title: Week 10
weight: 10
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-10/slides.html
---

## Overview

This week, we will learn about the surprisingly tricky task of quantifying biodiversity and biodiversity change.


Cover photo of an American Avocet from [AllAboutBirds](https://www.allaboutbirds.org/guide/American_Avocet/id).