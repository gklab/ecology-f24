---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 2
subtitle: ""
title: Week 06
weight: 6
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-06/slides.html
---

## Overview

This week, we dive deeper into analyzing the Lotka-Volterra model of competition,  a classic model from community ecology that seeks to explain coexistence between species with very similar resource requirements. We will learn to analyze equilibrium conditions in this model through Null-Cline analysis - a technique that we will see again when we learn about predator-prey interactions.

The [Weekly Activity](/pdf/Week-6-activity-questions.pdf) walks students through using an interactive app to generate insights from the Lotka-Volterra competition model. Though it is the same model, this activity helps students take a different analysis approach.


Cover photo by <a href="https://unsplash.com/@hiro0718?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Hiroko Yoshii</a> on <a href="https://unsplash.com/photos/9y7y26C-l4Y?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>
  