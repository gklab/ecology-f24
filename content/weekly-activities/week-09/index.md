---
date: "2021-01-01"
draft: false
type: collection
excerpt: Mid-semester check-in
subtitle: ""
title: Week 09
weight: 9
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-09/slides.html
---

## Overview



Cover photo by [Avel Chuklanov](https://unsplash.com/@chuklanov) on [Unsplash](https://unsplash.com/photos/HTyONr8k5TQ)