---
title: ""
format: 
  pdf:
    output-file: "survey"
geometry: bmargin=2cm

---


\vspace{-5em}

***Answer the following with 1-2 sentences***

1. Think back to the concepts/models we have covered in class so far:
- Mark-Recapture assays for population size estimation; Exponential and logistic population growth; Matrix population models; Inter- and intra-specific competition; Consumer-resource dynamics

Do you generally feel confident that you can explain the idea behind each of these concepts? Which one have you found most confusing so far?
\vspace{3em}

2. Is there any ecological concept that you are hoping we cover in the coming weeks of this course?
\vspace{3em}


***On a scale of 1 to 10, how much do you agree with the following statement*** (1 means strongly disagree; 10 means strongly agree):

1. I feel that the concepts we are covering in class help me understand the field of ecology  
\vspace{1.25em}
1. The lectures and biweekly activities are generally enough for me to feel confident about the course content
\vspace{1.25em}

1. The lack of exams reduces my motivation to learn the concepts we cover in class
\vspace{1.25em}
1. I enjoy doing the biweeklyv activities 
\vspace{1.25em}
1. The biweekly activities help me better understand the concepts we cover in class
\vspace{1.25em}
1. I wish the biweekly activities more specifically related to the concepts we cover in class
\vspace{1.25em}
1. I enjoy doing the biweekly reflections
\vspace{1.25em}
1. The biweekly reflections help me better understand the concepts we cover in class
\vspace{1.25em}
1. The biweekly reflections help me process my responsibilities and/or growth in general (beyond this class)
\vspace{1.25em}
1. The grading structure of this course seems fair to me
\vspace{1.25em}
1. The grading structure of this course motivates me to take charge of my learning
\vspace{1.25em}
1. The grading structure of this course makes it less likely that I will study the course material
\vspace{1.25em}
1. Five years from now, I am less likely to remember the material from this course than material I learn in other courses
\vspace{1.25em}
1. I feel that the Semester Project for this course is going to help me better understand how ecological concepts play out in nature
\vspace{1.25em}
1. I feel that the Semester Project for this course is going to help me develop skills that I can envision using beyond my undergrad career
\vspace{1.25em}
1. I feel excited to come to this course for in-person lectures
\vspace{1.25em}
1. I feel excited to come to complete the biweekly activities/biweekly reflections/semester project for this course
\vspace{1.25em}

***Answer the following with a few sentences***

1. What are some things you want me to know as the instructor of this course? Is there anything working particularly well, or particularly poorly for you?
\vspace{6em}


1. Reflect on your participation in this course. How often have you been attending lecture? Do you find lectures to be helpful, and are there changes that you would like to see implemented? 


