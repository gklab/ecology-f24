---
title: ""
format: 
  pdf:
    output-file: "Biweekly-activity-1-questions"
geometry: margin=2cm
---

\vspace{-20em}

## Biweekly activity 1: Population ecology and exponential growth

### Section 1: Measuring population sizes


Starting in the 1970s, Burmese Pythons - beautiful snakes that are native to south-east Asia - started becoming common imports into the US as part of the exotic pet trade. In the following decades, released pythons started establishing a population in the Everglades of Florida, and in the years since, their populations have grown rapidly, with devastating consequences for various native animals and whole ecosystems. Some experts have called the invasion of these pythons as "of the most intractable invasive-species management issues across the globe" ([Guzy et al., 2023](https://neobiota.pensoft.net/article/90439/)).

For more information about this issue, which please watch [this video](https://www.youtube.com/watch?v=kweD8y5LqsA). 
***Note: This video contains footage of snakes.***


1.1. You are tasked with estimating the population size of Burmese Pythons in the area surrounding Lake Okeechobee, which represents the northern limit of the population (as of 2020). Estimating python populations is notoriously difficult, and you decide to develop an estimate using the Mark-Recapture method.  

During your first week of field work, you capture and mark 7 pythons, which you release back into the population. A month later, you come back to the park and capture a total of 25 pythons, of which only 2 were marked. Based on this information, what is your estimate of the total population size in this area? Show your work. (1 point)


\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage

1.2. What are **two** assumptions of the Mark-Recapture method, and how might each assumption affect your estimate of the population size? In other words, do you think that violating each assumption would lead to an under-estimate or an over-estimate of population size? Would you expect the python population in your study site to violate either of these assumptions? (1 point)


\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage

### Section 2: Continuous population dynamics 

Recall the population dynamics equation for exponential growth:

$$\frac{dN}{dt} = rN$$

2.1. Consider three species that have exponential growth, one at a rate of $r_1 = 0.1$, the second at a rate of $r_2 = 0.25$, and the third at a rate of $r_3 = -0.05$. 

Assuming that all species have an initial population size of $N_{0} = 100$, sketch the trajectories of the two populations on the following graph. Make sure to label each population's trajectory. Note: You might find [this web-app](https://ecoevoapps.shinyapps.io/population_growth/) to be a useful resource. (1 point)

```{r echo = F, message = F, fig.height= 3, fig.width=4.5, fig.align='center'}
library(tidyverse)
ggplot(data = data.frame(x = 1:10, y = 1:10)) +
  geom_point(aes(x = x, y = y), size = 0, color = 'white') + 
  labs(x = "Time", y = "Population size") + 
  theme_classic() + 
  theme(axis.ticks = element_blank(), axis.text = element_blank())
```

\clearpage

### Section 3: Stage--structured population dynamics

This section of the activity centers around a paper that describes how basic population biology modeling has translated in to critical conservation action for endangered sea turtle populations. There are three components:

1. Watch [this video](https://www.youtube.com/watch?v=I5WM2wdjr1M) about Leslie Projection Matrices 
2. Read the paper "A Stage-Based Population Model for Loggerhead Sea Turtles and Implications for Conservation" ([Link](https://ecology.gklab.org/weekly-activities/week-03/Crouse_Ecology_1987.pdf)), and answer the questions below. 
3. Watch [this video](https://www.youtube.com/watch?v=pTEb-eSbUtQ) showing Turtle Excluder Devices at work.  

### *Questions about "A Stage-Based Population Model for Loggerhead Sea Turtles and Implications for Conservation"*

3.1. As of the time when this paper was written (1987), what was the primary focus of turtle conservation efforts? Why was this the primary focus of conservation activity? (1 point)


\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage

3.2. The authors simplify the turtle life cycle, which can extend more than 50 years, into just 7 different stages. Explain in your own words (a) why they chose to do this, (b) what these stages represent, and (c) the reproductive dynamics of these stages. (2 points)  

\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

3.3. In your own words, what does the "Elasticity" of a stage represent? What does Figure 4A tell us about the elasticity of different life stages? (2 points)

\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

\clearpage

3.4. If the authors had to pick a *single* life stage that should be the target of conservation, which one do you think they would advocate for? Why? (1 point)

\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

3.5. According to the authors, what is an uncomfortable possibility that conservation biologists needed to address about turtle conservation efforts? Where else do you think such lessons are applicable in our contemporary lives? (1 point)

\begin{Form}
\TextField[width=\linewidth, height=20em, bordercolor={0 0 0}, multiline=true]{\phantom{s}}
\end{Form}

