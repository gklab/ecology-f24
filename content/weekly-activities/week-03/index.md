---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 2
subtitle: ""
title: Week 03
weight: 2
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-03/slides.html
#- icon: pagelines
#  icon_pack: fab
#  name: Weekly reflection
#  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976690
#- icon: book
#  icon_pack: fa
#  name: Weekly Assignment
#  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2021909
- icon: book
  icon_pack: fa
  name: Activity 1 due
  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2483511
---

## Overview

In lecture this week, we will learn how to incorporate in biological reality in the form of age or stage-structure in populations. For example, some populations may have many individuals, but if most individuals are very old and have low reproductive output, the population may nevertheless decline to extinction in the long-term. On the other hand, small populations may eventually grow to be big if they mostly comprise individuals with high survival rates and high fecundity (reproductive rates). 



Featured image of a loggerhead sea turtle by <a href="https://unsplash.com/@adolfofelix?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Adolfo Félix</a> on <a href="https://unsplash.com/photos/turtle-swimming-on-body-of-water-BXN16VVFEio?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>:
  
<img src="https://images.unsplash.com/photo-1519515449982-8b104b672029?q=80&w=2670&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="loggerhead sea turtle" width="500"/>

