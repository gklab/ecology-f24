---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 4
subtitle: ""
title: Week 08
weight: 8
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-08/slides.html
---

## Overview

This week, we will learn about the role that consumer-resource interactions can have on ecological communities. 


The Semester Project Annotated Bibliography assignment is also due this week. 



Cover photo by [Avel Chuklanov](https://unsplash.com/@chuklanov) on [Unsplash](https://unsplash.com/photos/HTyONr8k5TQ)