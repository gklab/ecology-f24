---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 3
subtitle: ""
title: Week 04
weight: 4
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-04/slides.html
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=2483512
# - icon: book
#   icon_pack: fa
#   name: Weekly Assignment
#   url:  https://moodle.lsu.edu/mod/assign/view.php?id=2029563
- icon: hippo
  icon_pack: fa
  name: Semester Project update
  url:  /semester-project/semester-project-potential-communities.pdf
---

## Overview

In lecture this week, we will learn how to incorporate biological reality in the form of positive and negative density dependence. 

Featured photo of wild dogs by <a href="https://unsplash.com/@cooksphotography?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Hal Cooks</a> on <a href="https://unsplash.com/photos/a-group-of-wild-dogs-standing-next-to-each-other-pfb70hShQto?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>

![](https://images.unsplash.com/photo-1671931637734-6427ca008a44?q=80&w=2044&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)
  