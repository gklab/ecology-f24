---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 1
subtitle: ""
title: Week 05
weight: 5
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-05/slides.html
#- icon: pagelines
#  icon_pack: fab
#  name: Weekly reflection
#  url: https://moodle.lsu.edu/mod/assign/view.php?id=1976692
#- icon: book
#  icon_pack: fa
#  name: Weekly Assignment
#  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2038186
#- icon: hippo
#  icon_pack: fa
#  name: Semester Project update
#  url:  https://moodle.lsu.edu/mod/assign/view.php?id=2038133
---

## Overview

In lecture this week, we will turn to the field of community ecology, which investigates the relationships between different species that co-occur in the same habitat. We will begin with an overview of the different types of reactions that can take place between organisms, and then delve into ways of studying interactions between species that occupy the same *guild*. 


We will also spend some time this week focused on the semester project - the goal for this week is to explore potential communities that students can work in. 


Cover photo by <a href="https://unsplash.com/@pamelaheckel?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Pamela Heckel</a> on <a href="https://unsplash.com/photos/nGc7dxhp6IY?utm_source=unsplash&utm_medium=referral&utm_content=creditCopyText">Unsplash</a>

![](https://images.unsplash.com/photo-1681754289581-1219b6065261?q=80&w=2070&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D)
  