---
date: "2021-01-01"
draft: false
type: collection
excerpt: Community ecology, Part 3
subtitle: ""
title: Week 07
weight: 7
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-07/slides.html
---

## Overview

This week, we will wrap up some takeaways from the Lokta-Volterra competition model, and learn how lessons from this model help us understand species coexistence in more general contexts. Specifically, we will learn how insights from the Lotka-Volterra model help make sense of species coexistence mediated by organisms at other trophic levels (e.g. how herbivores or soil microbes can mediate plant-plant coexistence). Then, we will learn how similar models can help make sense of predator-prey interactions. 




Cover photo from [Wikimedia](https://upload.wikimedia.org/wikipedia/commons/6/65/Large_ground_finch_%284229035966%29.jpg)
  