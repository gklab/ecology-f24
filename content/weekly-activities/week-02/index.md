---
date: "2021-01-01"
draft: false
type: collection
excerpt: Population Ecology, part 1
subtitle: ""
title: Week 02
weight: 2
links:
- icon: puzzle-piece
  icon_pack: fas
  name: Reading 1
  url: week2/pdf/popln-ecology-reading.pdf
- icon: puzzle-piece
  icon_pack: fas
  name: Reading 2
  url: https://lsu.app.box.com/s/py9pj4cw8o8bk7hreombejpndrl223rr
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-02/slides.html
- icon: pagelines
  icon_pack: fab
  name: Self reflection due
  url: https://moodle.lsu.edu/mod/assign/view.php?id=2483509
---

## Overview

This week's lectures will provide an introduction to how ecologists model the process of populations growing or shrinking. We will begin with a simple model that considers a "closed"  population that only grows with births, and only shrinks with deaths. This gives rise to **exponential growth** dynamics, which form the basis for many other models in population ecology.

From here, we will add **demographic structure** to our populations by dividing the organisms into different age (or size) classes, and model their growth using matrix math. 


Featured photo of a starling murmuration by <a href="https://unsplash.com/@octopus_photo?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Pete Godfrey</a> on <a href="https://unsplash.com/photos/a-large-flock-of-birds-flying-over-a-body-of-water-jKNR--HDA_A?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash">Unsplash</a>:

<img src="https://images.unsplash.com/photo-1684793130560-ae91254db9ac?q=80&w=2071&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D" alt="starling murmuration" width="500"/>

    