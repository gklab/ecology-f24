---
date: "2021-01-01"
draft: false
type: collection
excerpt: What is ecology, and how do we do it?
subtitle: ""
title: Week 01
weight: 1
links:
# - icon: superpowers
#   icon_pack: fab
#   name: Slides
#   url: week1/slides.html
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-01/slides.html
- icon: puzzle-piece
  icon_pack: fas
  name: Reading
  url: week1/pdf/QuantEco-Ch1.pdf
- icon: pagelines
  icon_pack: fab
  name: Weekly reflection
  url: https://moodle.lsu.edu/mod/assign/view.php?id=2483509
- icon: binoculars
  icon_pack: fa
  name: Who's in class survey
  url: https://moodle.lsu.edu/mod/questionnaire/view.php?id=2483503
#- icon: dice-d20
#  icon_pack: fa
#  name: Mark-Recapture activity
#  url: pdf/mark-recapture-activity.pdf
---

## Overview

The goal for this week is introduce you to the Science of Ecology.

Monday's class will introduce students to the types of questions ecologists ask, the types of challenges we run into when searching for answers, and the ways in which ecologists address these challenges. 

On Wednesday, we will go over how the course is structured - what major topics we will cover, what your weekly responsibilities will be, and how grading will work for this course. We will also get a taste of some of the global biodiversity patterns and threats that puzzle and motivate ecologists. 

Friday's session will be built around an in-class activity to use the Mark-Recapture method for estimating population sizes.

## Reading

Before coming to class, please read Chapter 1 of [Quantitative Ecology](week1/pdf/QuantEco-Ch1.pdf). 

## Weekly reflection questions

The first Weekly Reflection for the semester is due via Moodle (click on the link above) at the end of Week 2. Here are some questions for you to consider for this reflection as you settle into the semester: 

### Reflections on ecology
- What does the word "ecology" mean to you? How do you think it is relevant to your life?
- What is your relationship with "nature"? In your mind, how can humans have a healthy relationship with nature?
- Is there a particular place that comes to your mind when you think of "being out in nature"? What kinds of characteristics define that space for you?

### Reflections on the semester
- What are some things you are excited about for this semester? What are some things you are nervous about?
- Looking at your semester schedule, are there going to be particular weeks that are looking busier (multiple exams/assignments due, etc.)? If so, how are you planning to prepare for these weeks?
- Two weeks into the semester, how do you see this course relating to the other courses you are taking, or to other activities in your life outside of this class?

<!--
### Thinking ahead to the semester project
- What places or ecological communities come to mind when you think about the Semester Project assignment for this course? What excites you about this place, and what might be potential stumbling blocks?
- What formats or media come to mind when you think  about the Semester Project for this course? What excites you about choosing this format? What are potential stumbling blocks along your way?--> 