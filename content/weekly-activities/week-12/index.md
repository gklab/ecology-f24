---
date: "2021-01-01"
draft: false
type: collection
excerpt: Ecosystem ecology, part 1
subtitle: ""
title: Week 12
weight: 12
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-12/slides.html
---

## Overview

On Monday, we will discuss last week's activity about island biogeography of soil microorganisms. On Wednesday, we will start thinking about patterns of nutrient dynamics at ecosystem scales, starting with a focus on the global water cycle. 

Cover photo of the rain shadow cast by the Andes in Bolivia from [Wikimedia Commons](https://en.wikipedia.org/wiki/Rain_shadow#/media/File:Satellite_image_of_Bolivia_in_June_2002.jpg)

