---
date: "2021-01-01"
draft: false
type: collection
excerpt: Ecosystem ecology, part 1
subtitle: ""
title: Week 13
weight: 13
links:
links:
- icon: superpowers
  icon_pack: fab
  name: Slides
  url: weekly-activities/week-13/slides.html
---

## Overview

On Monday, we will break out into small groups to discuss the semester project updates. On Wednesday and Friday, we will continue the Ecosystem Ecology module by turning our attention to the Nitrogen cycle. 

Cover photo of the rhizobial nodules on *Vicia* roots from [Wikimedia Commons](https://en.wikipedia.org/wiki/Fabaceae#/media/File:Vicia_sepium10_ies.jpg)

